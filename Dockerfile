# При каких-то проблемах можно просто оставить alpine и
# собирать с нуля, но это дольше
# FROM alpine
# FROM jasuramme/alpine-poetry
FROM alpine

RUN [ -d /sandbox ] || mkdir /sandbox

WORKDIR /sandbox

# Нам не откуда брать poetry.lock, так как запускаем
# на беслпатных CI/CD серверах, и они не будут хранить наш кэщ
# COPY poetry.lock /sandbox/

# копируем pyproject.toml из репозитория 
COPY pyproject.toml /sandbox/

# Устанавливаем зависимости для сборки модулей с помощью poetry
RUN apk --no-cache --update add \
    curl musl-dev linux-headers g++ python3 \
    python3-dev gfortran py-pip build-base
    
# делаем ссылку, что команда python это python3
RUN [ -f /bin/python ] || ln -s /usr/bin/python3 /bin/python
    
# Добавляем poetry в PATH
RUN [ -f $HOME/.poetry/bin/poetry ] || \
    echo export "PATH=\$PATH:\$HOME/.poetry/bin" >> /etc/profile 
    
# Устанавливаем poetry
RUN [ -f $HOME/.poetry/bin/poetry ] || \
    curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -

# Создаем виртуальное окружение
RUN python -m venv /sandbox/venv

# Обновляем pip
RUN /sandbox/venv/bin/pip install --upgrade pip

# Генерируем lock файл
RUN $HOME/.poetry/bin/poetry lock

# Устанавливаем зависимости poetry через бекэнд pip, иначе есть 
# проблемы с docker'ом, например см 
# https://github.com/python-poetry/poetry/issues/1937
RUN $HOME/.poetry/bin/poetry export -f requirements.txt | /sandbox/venv/bin/pip install --no-cache-dir -r /dev/stdin

# Можно удалить зависимости для сборки, это уменьшит размеры образа
# но тогда в случае чего образ не сможет обновиться
# RUN apk del curl musl-dev linux-headers g++ python3-dev \
# gfortran py-pip build-base

# Вход в образ делаем с --login, чтобы поднялся наш PATH
CMD ["sh", "--login"]
