# Alpine-poetry

Docker image for testing python project based on poetry

## Usage
This image include installed packages, which are defined in the pyproject.toml.

!IMPORTANT! In this build poetry just gives packeges names and versions and later on they are installing with pip. This is done such way because otherwise poetry will install all these packages from scratch for every new project. And by the way you cannot use poetry install without env, because it can conflict with linux repo distutils.

You shoul update packages from your CI/CD like that:

`poetry export -f requirements.txt | /sandbox/venv/bin/pip install -r /dev/stdin`

And activate that before start testing:

`source /sandbox/venv/bin/activate`

## Forking
If you want to fork this project and use on your own, you have to set up CI/CD variables, otherwise it will not able to push docker images to the dockerhub. 
You have to:
1) Get access token at the dockerhub (Security->New Access Token)
2) Put the following variables to the gitlab CI/CD settings (Settings -> CI/CD -> Variables)

- CI_REGISTRY -> docker.io
- CI_REGISTRY_IMAGE -> index.docker.io/DOCKER_USERNAME/image_name
- CI_REGISTRY_USER -> dockerhub user name
- CI_REGISTRY_TOKEN -> dockerhub access token

# Alpine-poetry

Docker образ для тестирования python проектов с использованием poetry

## Использование
Данный образ включает уже установленные через poetry пакеты. Они все перечислены в pyproject.toml. 

!ВАЖНО! poetry дает список пакетов, а устанавливаются они через pip, иначе для каждого нового проекта poetry будет начинать установку с нуля, причем это не лечится установкой пакетов без venv. 
Обновление пакетов в CI/CD должно выглядеть так:

`poetry export -f requirements.txt | /sandbox/venv/bin/pip install -r /dev/stdin`

потом, перед запуском проекта нужно сделать 

`source /sandbox/venv/bin/activate`

## Forking
Если вы хотите  сделать форк данного проекта, не забудьте, что gitlab CI/CD ипользует сохраненные в настройках профился явки и пароли для того, чтобы залить образ на dockerhub. Если вы хотите сделать это таким же образом, как у меня, то нужно сделать следующее:
1) Получить access токен в докерхабе (Security->New Access Token)
2) В настройках gitlab указать явки и пароли для доступа (Settings -> CI/CD -> Variables)

- CI_REGISTRY -> docker.io
- CI_REGISTRY_IMAGE -> index.docker.io/DOCKER_USERNAME/image_name
- CI_REGISTRY_USER -> имя пользователя на докерхабе
- CI_REGISTRY_TOKEN -> access токен
